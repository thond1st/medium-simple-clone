@extends('layouts.master')

@section('content')
    <style>
        body {
        padding-top: 80px;
        }

        .navbar-inverse {
        border-width: 0;
        background-color: #fff !important;
        color: #333; !important

        .navbar-nav {
            button {
            margin: 6px 0;
            }

            > li {
            margin: 0 5px;

            > a {
                color: #D2D7D3;
            }
            }

            .active > a, .active > a:focus {
            background-color: $body-bg;
            color: #333;
            }
        }
        }

        .navbar > .container .navbar-brand {
        color: #000;
        }

        #app {
        min-height: 600px;
        height: auto !important
        }

        .error-msg {
        ul {
            list-style: none;
            padding-left: 0;
        }
        }

        footer {
        margin-top: 20px;
        padding: 20px 0;
        background-color: #222;
        }

        .navbar-fixed-top2{
            top: 50px !important;
            position: fixed;
            border-radius: 0;
            right: 0;
            left: 0;
            z-index: 1030;
        }

        .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
            color: #333;
            background-color: #fff;
        }

        .navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus {
            color: #333;
            background-color: transparent;
        }
    </style>
    <div id="app" class="h-screen flex items-center justify-center bg-teal-lightest font-sans">

        @include('layouts.navigation')

        <div class="container">

            <router-view></router-view>

        </div>
    </div>

@endsection