const {mix} = require('laravel-mix');

let tailwindcss = require('tailwindcss');

mix.js('resources/assets/js/app.js', 'public/js')
   .extract(['vue'])
   .sass('resources/assets/sass/index.sass', 'public/css/app.css')
.options({
      processCssUrls: false,
      postCss: [ tailwindcss('./tailwind.js') ],
    })
   .copy("resources/assets/images", "public/images");


// LiveReload
var LiveReloadPlugin = require('webpack-livereload-plugin');
mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});